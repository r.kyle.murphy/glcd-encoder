extern crate image;
use image::Luma;
use std::path::PathBuf;
use clap::Clap;
use image::GrayImage;
use std::cmp;
use std::fs::File;
use std::io::BufReader;

#[derive(Clap, Debug, PartialEq)]
#[clap(version = env!("CARGO_PKG_VERSION"), author = env!("CARGO_PKG_AUTHORS"))]
struct Opts {
    #[clap(subcommand)]
    mode: Mode,
}

#[derive(Clap, Debug, PartialEq)]
enum Mode {
    /// encode a bitmap into a PROGMEM block suitable for insertion into C code
    Encode(Encode),
    /// decode the given byte array back into a bitmap
    Decode(Decode),
}

#[derive(Clap, Debug, PartialEq)]
enum Rotation {
    #[clap(alias = "r", alias = "90")]
    Right,
    #[clap(alias = "f", alias = "180")]
    Flip,
    #[clap(alias = "l", alias = "270")]
    Left,
}

#[derive(Clap, Debug, PartialEq)]
struct Encode {
    /// Path of image to convert, supports png, jpeg, gif, bmp, ico, tiff, dds, tga, and pnm
    #[clap(name = "IMAGE", parse(from_os_str))]
    bitmap: PathBuf,
    /// Horizontal offset within the encoded image to place this bitmap. If specified width is usually required
    #[clap(short)]
    x: Option<u32>,
    /// Vertical offset within the encoded image to place this bitmap. If specified height is usually required
    #[clap(short)]
    y: Option<u32>,
    /// Width of the encoded image, usually 128, defaults to width of the bitmap
    #[clap(short, long)]
    width: Option<u32>,
    /// Height of the encoded image, usually 64 or 32, defaults to height of the bitmap
    #[clap(short, long)]
    height: Option<u32>,
    /// Apply rotation to the image before encoding
    #[clap(arg_enum, short, long)]
    rotate: Option<Rotation>,
    /// Color to detect for background pixels. These will show up unlit when the image is rendered
    #[clap(arg_enum, short, long, name = "COLOR", default_value = "black")]
    background: BackgroundColor,
}

#[derive(Clap, Debug, PartialEq)]
enum BackgroundColor {
    #[clap(alias = "w")]
    White,
    #[clap(alias = "b")]
    Black,
}

#[derive(Clap, Debug, PartialEq)]
struct Decode {
    /// Path to file containing a json array of the bytes to decode
    #[clap(parse(from_os_str))]
    json: PathBuf,
    /// Name of image file to write output to. Supports png, jpeg, gif, bmp, ico, tiff, and pnm
    #[clap(short, long, parse(from_os_str), default_value="decoded.png")]
    output: PathBuf,
    /// width of the encoded image, usually this is 128
    #[clap(short, long, default_value = "128")]
    width: u32,
    /// Height of the image. If not specified it will be derived from the width and the number of bytes in the input
    #[clap(short, long)]
    height: Option<u32>,
    /// Rotation to apply to the extracted image before saving
    #[clap(arg_enum, short, long)]
    rotate: Option<Rotation>,
    // Color to write for background pixels, foreground will be written as the opposite
    #[clap(arg_enum, short, long, name = "COLOR", default_value = "black")]
    background: BackgroundColor,
}

fn main() {
    let opts = Opts::parse();
    match opts.mode {
        Mode::Encode(opts) => encode(opts),
        Mode::Decode(opts) => decode(opts),
    }
}

fn encode(opts: Encode) {
    let bmp: GrayImage = image::open(opts.bitmap).unwrap().into_luma();
    let adjusted_image = match opts.rotate {
        Some(Rotation::Right) => image::imageops::rotate90(&bmp),
        Some(Rotation::Flip) => image::imageops::rotate180(&bmp),
        Some(Rotation::Left) => image::imageops::rotate270(&bmp),
        None => bmp,
    };
    let (image_width, image_height) = adjusted_image.dimensions();
    let width = opts.width.unwrap_or(image_width);
    let height = opts.height.unwrap_or(image_height);
    let x_offset = opts.x.unwrap_or(0);
    let y_offset = opts.y.unwrap_or(0);
    let padding = height % 8;
    let color = match opts.background {
        BackgroundColor::Black => u8::MIN,
        BackgroundColor::White => u8::MAX,
    };
    let packed_rows = height.checked_shr(3).unwrap_or(padding) + cmp::min(padding, 1);
    println!("// {} by {} image, {} bytes total", width, packed_rows * 8, width * packed_rows);
    if packed_rows * 8 > height {
        println!("// WARNING! Requested image height not aligned to 8 byte boundary, padding to {} rows to align", packed_rows * 8);
    }
    println!("static const char PROGMEM image[] = {{");
    for y in 0..packed_rows {
        for x in 0..width {
            let mut byte = 0u8;
            'row: for row_offset in 0..8 {
                // If the current for and column are outside the image placement area, continue
                if x < x_offset 
                    || x - x_offset > image_width - 1 
                    || (y * 8) + row_offset < y_offset 
                    || (y * 8) + row_offset - y_offset > image_height - 1 {
                    continue 'row;
                }
                // Calculate the current X,Y in the images coordinate space
                let translated_x = x - x_offset;
                let translated_y = (y*8) + row_offset - y_offset;
                assert!(translated_x < image_width);
                assert!(translated_y < image_height);
                let image::Luma(intensity) = adjusted_image[(translated_x, translated_y)];
                // Set this bit if this is a non background pixel
                if intensity[0] != color {
                    let mask = 1u8.checked_shl(row_offset).unwrap();
                    byte = byte | mask;
                }
            }
            // Skip comma on the very last entry because C is picky that way
            if y == packed_rows - 1 && x == width - 1 {
                print!("{} ", byte);
            } else {
                print!("{}, ", byte);
            }
        }
        println!("// row {} to {}", (y * 8) + 1, ((y + 1) * 8));
    }
    println!("}};")
}

fn decode(opts: Decode) {
    let file = File::open(opts.json).expect("Failed to open JSON file!");
    let reader = BufReader::new(file);
    let bytes: Vec<u8> = serde_json::from_reader(reader).unwrap();
    let width = opts.width;
    let height = opts.height.unwrap_or_else(|| (bytes.len() * 8) as u32 / width);
    let mut buffer = image::GrayImage::new(width, height);
    let foreground = match opts.background {
        BackgroundColor::Black => u8::MAX,
        BackgroundColor::White => u8::MIN,
    };
    let background = match opts.background {
        BackgroundColor::Black => u8::MIN,
        BackgroundColor::White => u8::MAX,
    };

    let mut underflowed = 0;

    for x in 0..width {
        for y in 0..height {
            let index = (x + (y.checked_shr(3).unwrap_or(0) * width)) as usize;
            let byte = if index < bytes.len() { 
                bytes[index] 
            } else { 
                underflowed += 1;
                background 
            };
            let row_mask = 1u8.checked_shl(y % 8).unwrap_or(0);
            let bit = byte & row_mask;
            let color = if bit > 0 { foreground } else { background };
            buffer.put_pixel(x, y, Luma([color]))
        }
    }
    let adjusted_image = match opts.rotate {
        Some(Rotation::Right) => image::imageops::rotate90(&buffer),
        Some(Rotation::Flip) => image::imageops::rotate180(&buffer),
        Some(Rotation::Left) => image::imageops::rotate270(&buffer),
        None => buffer,
    };

    adjusted_image.save(opts.output).expect("Failed to save output!");
    if underflowed > 0 {
        println!("Warning, missing {} bytes in input for requested image size, remaining space filled with background color!", underflowed / 8);
    }
}
