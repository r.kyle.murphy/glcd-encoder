# glcd-encoder

Utility to encode/decode monochrome images into the format used by SSD1306 OLED panels (and possibly others).

Should build by simply running `cargo build`.